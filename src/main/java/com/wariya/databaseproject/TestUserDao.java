/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.wariya.databaseproject;

import com.wariya.databaseproject.model.User;
import com.wariya.databaseproject.dao.UserDao;
import com.wariya.databaseproject.helper.DatabaseHelper;

/**
 *
 * @author wariy
 */
public class TestUserDao {

    public static void main(String[] args) {
        UserDao userDao = new UserDao();
        for (User u : userDao.getAll()) {
            System.out.println(u);
        }
//        User user1 = userDao.get(2);
//        System.out.println(user1);

//        User newUser = new User("user3","password", 1, "F");
//        User insertedUser = userDao.save(newUser);
//        System.out.println(insertedUser);
//        insertedUser.setGender("M");
//        user1.setGender("F");
//        userDao.update(user1);
//        User updateUser = userDao.get(user1.getId());
//        System.out.println(updateUser);
//
//        userDao.delete(user1);
//        for (User u : userDao.getAll()) {
//            System.out.println(u);
//        }

        for(User u : userDao.getAll("user_name like 'u%' ", "user_name asc, user_gender desc")) {
            System.out.println(u);
        }
        DatabaseHelper.close();
    }
}

