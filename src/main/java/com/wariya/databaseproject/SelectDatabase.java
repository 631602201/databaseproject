/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.wariya.databaseproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;




/**
 *
 * @author wariy
 */
public class SelectDatabase {

    public static void main(String[] args) {
        Connection con = null;
        String url = "jdbc:sqlite:dcoffee.db";
        //connect
        try
        {
            con = DriverManager.getConnection(url);
            System.out.println("Connection to Sqlite has been establish.");
        } catch (SQLException ex)
        {
            System.out.println(ex.getMessage());
            return;
        }

        //select
        String sql = "Select * FROM category ";
        try
        {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next())
            {
                System.out.println(rs.getInt("category_id") + " " + rs.getString("category_name"));
            }
        } catch (SQLException ex)
        {
            System.out.println(ex.getMessage());
        }

        //close
        if (con != null)
        {
            try
            {
                con.close();
            } catch (SQLException ex)
            {
                System.out.println(ex.getMessage());
            }
        }
    }
}



