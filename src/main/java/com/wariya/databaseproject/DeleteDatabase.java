/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.wariya.databaseproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


/**
 *
 * @author wariy
 */
public class DeleteDatabase {
    public static void main(String[] args) {
        Connection con = null;
        String url = "jdbc:sqlite:dcoffee.db";
        //connect
        try
        {
            con = DriverManager.getConnection(url);
            System.out.println("Connection to Sqlite has been establish.");
        } catch (SQLException ex)
        {
            System.out.println(ex.getMessage());
            return;
        }

        //Insert
        String sql = "DELETE FROM category WHERE category_id = ?";
        try
        {
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, 7);
            int status = stmt.executeUpdate();
            //ResultSet key = stmt.getGeneratedKeys();
            //key.next();
            //System.out.println("" + key.getInt(1));

        } catch (SQLException ex)
        {
            System.out.println(ex.getMessage());
        }

        //close
        if (con != null)
        {
            try
            {
                con.close();
            } catch (SQLException ex)
            {
                System.out.println(ex.getMessage());
            }
        }
    }
}

