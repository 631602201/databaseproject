/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.wariya.databaseproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author wariy
 */
public class ConnectDatabase {

    public static void main(String[] args) {
       Connection con = null;
        String url = "jdbc:sqlite:dcoffee1.db";
        try
        {
            con = DriverManager.getConnection(url);
            System.out.println("Connection to Sqlite has been establish.");
        } catch (SQLException ex)
        {
            System.out.println(ex.getMessage());
        }finally {
            if(con != null){
                try
                {
                    con.close();
                } catch (SQLException ex)
                {
                    System.out.println(ex.getMessage());
                }
            }

        }

    }
}
